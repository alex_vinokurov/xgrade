const Express = require('express');
const bodyParser = require('body-parser');

const API_PORT = process.env.API_PORT || 4201;

const api = new Express();


api.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

api.use(bodyParser.urlencoded({
  extend: true
}));
api.use(bodyParser.json());

api.use('/api/list', require('./routes/list'));

api.use((req, res) => {
  res.status(404).end('Not found');
});


if (require.main === module) {
  api.listen(API_PORT, () => {
    console.log(`API server is now listening to http://localhost:${API_PORT}`);
  });
} else {
  module.exports = api;
}
