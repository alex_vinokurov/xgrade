const MongoClient = require('mongodb').MongoClient;

function connect(db='xgrade') {
  return new Promise((resolve, reject) => {
    MongoClient.connect(`mongodb://app:xxx67136@localhost:27017/${db}?authSource=admin`, (err, conn) => {
      if (err)
        reject(err);
      resolve(conn);
    });
  });
}

// function insert(collection, data) {
//   return new Promise((resolve, reject) => {
//     if (!Array.isArray(data))
//       data = [data];
//     collection.insertMany(data, (err, res) => {
//       if (err)
//         reject(err);
//       resolve(res);
//     });
//   });
// }

function find(collection, filter = {}, sort = {}) {
  return new Promise((resolve, reject) => {
    collection.find(filter).sort(sort).toArray((err, res) => {
      if (err)
        reject(err);
      resolve(res);
    });
  });
}

function findOne(collection, filter={}) {
  return new Promise((resolve, reject) => {
    collection.findOne(filter, (err, res) => {
      if (err)
        reject(err);
      resolve(res);
    });
  });
}

function update(collection, filter, data) {
  return new Promise((resolve, reject) => {
    collection.updateOne(filter, {$set: data}, (err, res) => {
      if (err)
        reject(err);
      resolve(res);
    });
  });
}

// function del(collection, filter) {
//   return new Promise((resolve, reject) => {
//     collection.deleteOne(filter, (err, res) => {
//       if (err)
//         reject(err);
//       resolve(res);
//     });
//   });
// }

module.exports = {
  connect,
  // insert,
  find,
  findOne,
  update,
  // del
};
