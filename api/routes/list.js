const Express = require('express');
const mongo = require('../mongo_utils');
const _ = require('lodash');

const col = 'test';

const list = new Express();

list.get('/', async (req, res) => {
  try {
    const conn = await mongo.connect();
    const data = await mongo.find(conn.collection(col), {active: true}, {order: 1});
    res.json(_.map(data, test => ({id: test.id, name: test.name, description: test.description, counter: test.counter})));
    conn.close();
  } catch (err) {
    res.status(500).end(err.message);
  }
});

list.get('/:testId', async (req, res) => {
  try {
    const conn = await mongo.connect();
    const testId = req.params.testId;
    const data = await mongo.findOne(conn.collection(col), {id: testId});
    data.questions = _.sampleSize(_.get(data, 'questions'), _.get(data, 'config.count', 15));
    res.json(data);
    conn.close();
  } catch (err) {
    res.status(500).end(err.message);
  }
});

list.post('/:testId', async (req, res) => {
  const testId = req.params.testId;
  const data = req.body;

  if (!data) {
    res.status(400).end('Bad Request');
  }

  try {
    const conn = await mongo.connect();
    await mongo.update(conn.collection(col), {id: testId}, data);
    conn.close();
    res.json({accepted: true});
  } catch (err) {
    res.status(500).end(err.message);
  }
});

module.exports = list;
