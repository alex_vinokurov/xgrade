import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, Router} from '@angular/router';
import {QuizService} from '../services/quiz.service';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

@Injectable()
export class QuizListResolver implements Resolve<any> {
  constructor(private quizService: QuizService) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.quizService.getAll();
  }
}

@Injectable()
export class QuizResolver implements Resolve<any> {
  constructor(private quizService: QuizService, private router: Router) {}
  resolve(route: ActivatedRouteSnapshot): Observable<any> {
    return this.quizService.get(route.params.id).pipe(catchError(err => {
      this.router.navigate(['']);
      return EMPTY;
    }));
  }
}
