import { QuizConfig } from './quiz-config';
import { Question } from './question';

export class Quiz {
    id: string;
    name: string;
    description: string;
    config: QuizConfig;
    themes: string[];
    questions: Question[];
    counter: number;

    constructor(data: any) {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.config = data.config || null;
            const themes = data.themes || [];
            this.themes = themes.join(', ');
            this.questions = [];
            data.questions.forEach(q => {
                this.questions.push(new Question(q));
            });
            this.counter = data.counter;
        }
    }
}
