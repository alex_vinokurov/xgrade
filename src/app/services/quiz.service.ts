import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class QuizService {

  constructor(private http: HttpClient) { }

  get(id: string) {
    return this.http.get(`${environment.apiRoot}/list/${id}`);
  }

  getAll() {
    return this.http.get(`${environment.apiRoot}/list`);
  }

  updateCounter(id: string, counter: number) {
    return this.http.post(`${environment.apiRoot}/list/${id}`, {counter});
  }

}
