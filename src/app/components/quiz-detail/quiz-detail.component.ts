import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quiz-detail',
  templateUrl: './quiz-detail.component.html',
  styleUrls: ['./quiz-detail.component.css']
})
export class QuizDetailComponent implements OnInit {
  quizId: string;
  quizName: string;
  quizDetailContent: string[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.quizId = this.route.snapshot.data.quiz.id;
    this.quizName = this.route.snapshot.data.quiz.name;
    this.quizDetailContent = this.route.snapshot.data.quiz.details;
  }

}
