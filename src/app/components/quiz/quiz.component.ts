import {Component, OnInit, Inject, PLATFORM_ID, OnDestroy} from '@angular/core';
import { isPlatformBrowser, isPlatformServer } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

import { QuizService } from '../../services/quiz.service';
import { Option, Question, Quiz, QuizConfig } from '../../models';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css'],
  providers: [QuizService]
})
export class QuizComponent implements OnInit, OnDestroy {
  quiz: Quiz = new Quiz(null);
  mode = 'quiz';
  config: QuizConfig = {
    'allowBack': true,
    'allowReview': true,
    'autoMove': false,
    'duration': 600,
    'pageSize': 1,
    'requiredAll': false,
    'richText': false,
    'shuffleQuestions': false,
    'shuffleOptions': false,
    'showClock': false,
    'showPager': true,
    'theme': 'none'
  };

  pager = {
    index: 0,
    size: 1,
    count: 1
  };
  timer: any = null;
  startTime: Date;
  endTime: Date;
  ellapsedTime = '00:00';
  duration = '';

  languages = ['javascript', 'typescript', 'python'];

  constructor(@Inject(PLATFORM_ID)
              private platform: Object,
              private quizService: QuizService,
              private toastr: ToastrService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.mode = 'quiz';
     this.quiz = new Quiz(this.route.snapshot.data.quiz);
      if (this.quiz.config) {
        this.config = this.quiz.config;
      }
      this.pager.count = this.quiz.questions.length;
      if (isPlatformBrowser(this.platform)) {
        this.startTime = new Date();
        this.ellapsedTime = '00:00';
        this.timer = setInterval(() => { this.tick(); }, 1000);
        this.duration = this.parseTime(this.config.duration);
      }
  }

  ngOnDestroy() {
    if (this.timer) {
      clearInterval(this.timer);
    }
  }

  tick() {
    const now = new Date();
    const diff = (now.getTime() - this.startTime.getTime()) / 1000;
    if (diff >= this.config.duration) {
      if (this.timer) {
      clearInterval(this.timer);
    }
      this.toastr.warning('Время на выполнение теста истекло!');
      this.onSubmit();
    }
    this.ellapsedTime = this.parseTime(diff);
  }

  parseTime(totalSeconds: number) { // Переделать на moment
    let mins: string | number = Math.floor(totalSeconds / 60);
    let secs: string | number = Math.round(totalSeconds % 60);
    mins = (mins < 10 ? '0' : '') + mins;
    secs = (secs < 10 ? '0' : '') + secs;
    return `${mins}:${secs}`;
  }

  get filteredQuestions() {
    return (this.quiz.questions) ?
      this.quiz.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
  }

  onSelect(question: Question, option: Option) {
    if (question.questionTypeId !== 1) {
      question.options.forEach((x) => { if (x.id !== option.id) x.selected = false; });
    }
    //
    // if (this.config.autoMove) {
    //   this.goTo(this.pager.index + 1);
    // }
  }

  goTo(index: number) {
    if (index >= 0 && index < this.pager.count) {
      this.pager.index = index;
      this.mode = 'quiz';
    }
  }

  isAnswered(question: Question) {
    return !!question.options.find(x => x.selected);
  }

  isCorrect(question: Question) {
    return question.options.every(x => !!x.selected === x.isAnswer);
  }

  get quizResult() {
    let scores = 0;
    const scoresTotal = this.quiz.questions.length;
    this.quiz.questions.forEach(question => {
      if (this.isCorrect(question)) {
        scores++;
      }
    });
    const rate = scores / scoresTotal;
    let resultClass: string;
    if (rate < 0.5) {
      resultClass = 'alert-danger';
    } else if (rate < 0.75) {
      resultClass = 'alert-warning';
    } else {
      resultClass = 'alert-success';
    }
    return {
      scoresTotal,
      scores,
      resultClass,
    };
  }

  onSubmit() {
    const answers = [];
    this.quiz.questions.forEach(question => answers.push({
      'quizId': this.quiz.id,
      'questionId': question.id,
      'answered': question.answered,
    }));
    this.quizService.updateCounter(this.quiz.id, this.quiz.counter + 1).subscribe(() => {
      this.mode = 'result';
    });
  }

  get disableNextQuestion() {
    return this.pager.index === this.pager.count - 1;
  }
}
