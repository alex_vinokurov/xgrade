import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { QuizComponent } from './components/quiz/quiz.component';
import { ListComponent } from './components/list/list.component';
import { QuizListResolver, QuizResolver } from './resolvers/quiz.resolver';
import { QuizDetailComponent } from './components/quiz-detail/quiz-detail.component';

const appRoutes: Routes = [
  { path: 'test/:id',
    component: QuizComponent,
    resolve: {
      quiz: QuizResolver
      }
  },
  { path: 'test-detail/:id',
    component: QuizDetailComponent,
    resolve: {
      quiz: QuizResolver
      }
  },
  { path: '',
    component: ListComponent,
    resolve: {
      quizList: QuizListResolver
      }
    },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {
    initialNavigation: 'enabled',
  })],
  exports: [RouterModule],
  providers: [
    QuizListResolver,
    QuizResolver,
  ]
})
export class AppRoutingModule {}
